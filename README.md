# Equatorial mount stepper driver

This firmware targets bluepill (stm32) hardware and will drive a unipolar stepper motor with PWM.
Use some capacitors to smooth the PWM.

The signals will be output on pins A0, A1, A2, A4.

**Tuning the stepper:**
Knowing the target period T (around 3,523s for a 4076 steps motor), one has to adjust both:
- resolution of the signal (number of values in the table): *R*
- frequency of duty cycle update: *F* (Hz)

We have the following equality: T = R/F

So for a frequency of 1200Hz (hardcoded for now), you have to take R = T.F = 4228 values.

## Development environment

Use [nix](https://nixos.org/download.html):
`nix shell`

## Building

This is a classic rust project, run:
`cargo build`

Signal values are computed at build time, you can adjust both resolution and max value with environment variables:

- `RESOLUTION` defaults to 1050
- `MAX_DUTY` defaults to 2000

eg:
`RESOLUTION=1050 MAX_DUTY=2000 cargo build`
