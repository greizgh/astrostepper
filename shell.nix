let
  moz_overlay = import (builtins.fetchTarball {
    url = "https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz";
  });
  nixpkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
in with nixpkgs;
stdenv.mkDerivation {
  name = "equastepper";
  buildInputs = [
    (rustChannels.stable.rust.override {
      targets = [ "thumbv7m-none-eabi" ];
      extensions = [ "rust-std" "rustfmt-preview" ];
    })
    gcc-arm-embedded
    stlink
  ];
}
