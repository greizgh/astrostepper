use std::env;
use std::f32::consts::PI;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

const VALUES: &str = r#"
// Analog signal wave resolution is contrained to this length
// This makes adjusting clocks tricky :/
const BUF_LEN: usize = <RESOLUTION>;

const DUTY_VALUES: [u16; BUF_LEN] = [<VALUES>];
"#;

fn gen_values(res: usize, max_duty: u16) -> Vec<u16> {
    let half_period = res / 2;
    let mut sine: Vec<u16> = [0].repeat(res);

    for i in 1..half_period {
        sine[i] = (max_duty as f32 * (PI * i as f32 / half_period as f32).sin()).round() as u16;
    }

    sine
}

fn main() {
    let resolution: usize = if let Ok(res) = env::var("RESOLUTION") {
        res.parse().unwrap_or(1050)
    } else {
        1050
    };
    println!("cargo:rerun-if-env-changed=RESOLUTION");
    let max_duty: u16 = if let Ok(res) = env::var("MAX_DUTY") {
        res.parse().unwrap_or(2000)
    } else {
        2000
    };
    println!("cargo:rerun-if-env-changed=MAX_DUTY");
    // Put the linker script somewhere the linker can find it
    let out = &PathBuf::from(env::var_os("OUT_DIR").unwrap());
    File::create(out.join("memory.x"))
        .unwrap()
        .write_all(include_bytes!("memory.x"))
        .unwrap();
    println!("cargo:rustc-link-search={}", out.display());

    // Only re-run the build script when memory.x is changed,
    // instead of when any part of the source code changes.
    println!("cargo:rerun-if-changed=memory.x");

    File::create(out.join("values.rs"))
        .unwrap()
        .write_all(
            VALUES
                .replace("<RESOLUTION>", &format!("{}", resolution))
                .replace(
                    "<VALUES>",
                    &gen_values(resolution, max_duty)
                        .iter()
                        .map(|v| v.to_string())
                        .collect::<Vec<String>>()
                        .join(","),
                )
                .as_bytes(),
        )
        .unwrap();
    println!("cargo:rerun-if-changed=build.rs");
}
