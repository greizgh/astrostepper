#![no_std]
#![no_main]

use core::cell::RefCell;
use cortex_m::{asm, interrupt::Mutex};
use cortex_m_rt::entry;
use stm32f1xx_hal as hal;

#[allow(unused_imports)]
use panic_halt;

use crate::hal::{
    gpio::gpioa::{PA0, PA1, PA2, PA3},
    gpio::{Alternate, PushPull},
    pac,
    pac::{interrupt, TIM3},
    prelude::*,
    pwm::{Channel, Pwm},
    timer::{CountDownTimer, Event, Tim2NoRemap, Timer},
};

mod table;
use table::MicrostepTable;

// I'm not really proud of this :/
type PWMDEF = Pwm<
    pac::TIM2,
    Tim2NoRemap,
    (hal::pwm::C1, hal::pwm::C2, hal::pwm::C3, hal::pwm::C4),
    (
        PA0<Alternate<PushPull>>,
        PA1<Alternate<PushPull>>,
        PA2<Alternate<PushPull>>,
        PA3<Alternate<PushPull>>,
    ),
>;

// Make timer interrupt registers globally available
static G_TIM: Mutex<RefCell<Option<CountDownTimer<TIM3>>>> = Mutex::new(RefCell::new(None));
// Make PWM globally available
static G_PWM: Mutex<RefCell<Option<PWMDEF>>> = Mutex::new(RefCell::new(None));

static G_TBL: Mutex<RefCell<Option<MicrostepTable>>> = Mutex::new(RefCell::new(None));

/// update PWM duty for all 4 outputs
#[interrupt]
fn TIM3() {
    static mut PWM: Option<PWMDEF> = None;
    static mut TIM: Option<CountDownTimer<TIM3>> = None;
    static mut TBL: Option<MicrostepTable> = None;

    let pwm = PWM.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| {
            // Move PWM pin here, leaving a None in its place
            G_PWM.borrow(cs).replace(None).unwrap()
        })
    });

    let tim = TIM.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| G_TIM.borrow(cs).replace(None).unwrap())
    });

    let tbl = TBL.get_or_insert_with(|| {
        cortex_m::interrupt::free(|cs| G_TBL.borrow(cs).replace(None).unwrap())
    });

    let (d1, d2, d3, d4) = tbl.next_values();
    pwm.set_duty(Channel::C1, d1);
    pwm.set_duty(Channel::C2, d2);
    pwm.set_duty(Channel::C3, d3);
    pwm.set_duty(Channel::C4, d4);

    let _ = tim.wait();
}

#[entry]
fn main() -> ! {
    let p = pac::Peripherals::take().unwrap();

    let mut flash = p.FLASH.constrain();
    let mut rcc = p.RCC.constrain(); // RCC: Reset and Clock Control

    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut afio = p.AFIO.constrain(&mut rcc.apb2);

    let table = MicrostepTable::default();
    cortex_m::interrupt::free(|cs| *G_TBL.borrow(cs).borrow_mut() = Some(table));

    // Define PWM outputs
    let mut gpioa = p.GPIOA.split(&mut rcc.apb2);

    let c1 = gpioa.pa0.into_alternate_push_pull(&mut gpioa.crl);
    let c2 = gpioa.pa1.into_alternate_push_pull(&mut gpioa.crl);
    let c3 = gpioa.pa2.into_alternate_push_pull(&mut gpioa.crl);
    let c4 = gpioa.pa3.into_alternate_push_pull(&mut gpioa.crl);

    let pins = (c1, c2, c3, c4);

    let mut pwm = Timer::tim2(p.TIM2, &clocks, &mut rcc.apb1).pwm::<Tim2NoRemap, _, _, _>(
        pins,
        &mut afio.mapr,
        4.khz(),
    );

    // Start at 0 duty
    for channel in &[Channel::C1, Channel::C2, Channel::C3, Channel::C4] {
        pwm.set_duty(*channel, 0);
        pwm.enable(*channel);
    }

    // Make pwm globally available
    cortex_m::interrupt::free(|cs| *G_PWM.borrow(cs).borrow_mut() = Some(pwm));

    // Setup duty cycle update timer
    // Table has 4200 values, period is 3.5s -> 1/(3.5/4200) = 1200Hz
    let mut timer = Timer::tim3(p.TIM3, &clocks, &mut rcc.apb1).start_count_down(1200.hz());

    // Generate an interrupt when the timer expires
    timer.listen(Event::Update);
    cortex_m::interrupt::free(|cs| *G_TIM.borrow(cs).borrow_mut() = Some(timer));

    unsafe {
        cortex_m::peripheral::NVIC::unmask(pac::Interrupt::TIM3);
    }

    loop {
        asm::wfi();
    }
}
