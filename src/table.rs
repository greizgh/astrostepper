type Duty = u16;
type ChannelsDuty = (Duty, Duty, Duty, Duty);

include!(concat!(env!("OUT_DIR"), "/values.rs"));

// Each channel is dephased by π/2
const OFFSET: usize = BUF_LEN / 4;

/// Shift position by increment, wrapping if at the end of the buffer
fn shift_pos(pos: usize, increment: usize) -> usize {
    (pos + increment) % (BUF_LEN - 1)
}

/// Table of values, approximation of:
/// sin(t) if t<= pi*T/2
/// 0 if t>pi*T/2
pub struct MicrostepTable {
    /// Position of the 4 channels
    pos: (usize, usize, usize, usize),
}

impl Default for MicrostepTable {
    fn default() -> Self {
        Self {
            pos: (0, OFFSET, 2 * OFFSET, 3 * OFFSET),
        }
    }
}

impl MicrostepTable {
    fn move_pos(&mut self) {
        self.pos.0 = shift_pos(self.pos.0, 1);
        self.pos.1 = shift_pos(self.pos.1, 1);
        self.pos.2 = shift_pos(self.pos.2, 1);
        self.pos.3 = shift_pos(self.pos.3, 1);
    }
    pub fn next_values(&mut self) -> ChannelsDuty {
        let idx = self.pos;
        self.move_pos();
        (
            DUTY_VALUES[idx.0],
            DUTY_VALUES[idx.1],
            DUTY_VALUES[idx.2],
            DUTY_VALUES[idx.3],
        )
    }
}
